---
title: Little Office of the Immaculate Conception
aside: true
---

The Little Office of the Immaculate Conception is an even more scaled down version of the Little Office of the Blessed Virgin Mary, which in turn is a scaled down version of the Divine Office with a focus on Mary, the Mother of God.

This Little Office sprang up in the 1600s.

## The Words

Here is a really nice 12 page section containing the Little Office of the Immaculate Conception in Latin and English from [The Sodalist's hymnal](https://archive.org/details/sodalistshymnalc00macg_0/page/358/mode/2up), Edwin F MacGonigle, 1887.

<a href="/pdf/officiumparvumImmac.pdf" class="button">Little Office of the Immaculate Conception in Latin and English Text</a>

And if your computer doesn't automatically do booklet printing, try [this pdf, flip long side](/pdf/officiumparvumImmac-book.pdf).

[Here is Michael Martin's page with all the prayers in Latin and English](https://www.preces-latinae.org/thesaurus/BVM/OPConImm.html)

## The Music

This section can be divided into the Chant Tune for the Latin, Metrical Tunes for the Latin and Metrical Tunes for the English.

### The Chant Tune

[This page](https://www.sanctamissa.pl/en/prayer-and-hours/officium-de-immaculata-conceptione-b-v-mariae-little-hours-to-the-immaculate-conception-of-the-blessed-virgin-mary/) from sanctamissa.pl gives audio recordings of all the hours. It is a fantastic collection, but doesn't give any sheet music, just the text and the recordings.

It is very close to the tune sung in [this video](https://www.youtube.com/watch?v=QLisXhdQfV8).

<a href="/pdf/immac/chant-tune.pdf" class="button">Chant tune, 5 page PDF</a>

In [this video](https://youtu.be/JWCly-RaEn8) I refer to [this booklet](/pdf/immac/qed.pdf) where I arranged the chant in four line notation and also show the tune from [sanctamissa.pl](https://www.sanctamissa.pl/en/prayer-and-hours/officium-de-immaculata-conceptione-b-v-mariae-little-hours-to-the-immaculate-conception-of-the-blessed-virgin-mary/)

Listen to our renditions in the [Youtube Playlist](https://www.youtube.com/watch?v=OVx-wBoDUWo&list=PLR7cOUX2gl7rFRShjbwRMgC1qCbqrtokf)

### Other Tunes for the Latin

The [Brebeuf Hymnal](https://ccwatershed.org/hymn) pairs this hymn with no fewer than five (5) tunes. 

1. Gaudeamus Pariter or Ave Virgo Virginum by Jan Roh. [Hymnary.org](https://hymnary.org/tune/gaudeamus_pariter_roh) has copious examples.
2. Good King Wenceslas, which turns out to be an older tune called Tempus Adest Floridum
3. Christus Christus Christus Ist by Peter Franck
4. Jesu Leiden Pein Und Tod, Melchior Vulpius
5. Ach Wie Kurz, J H Reiman

### Tunes for the English

There is a lovely metrical English translation which fits the widely known Lourdes Hymn.

Once you have learned a tune which fits the hymn, the rest will be mostly simple tones for collects and versicles. The hymn appears in a few hymnbooks, including three that I happen to have prepared reprints of through Lulu:

* [St Basil's Hymnal](https://shop.jubil.us/product/st-basils-hymnal-1918/)
* [A Treasury of Catholic Song](https://shop.jubil.us/product/treasury-of-catholic-song/)
* [Arundel Hymns](https://shop.jubil.us/product/arundel-hymns/)


St Basil's uses a simple tune. [Here is the relevant page from the Internet Archive](https://ia801603.us.archive.org/view_archive.php?archive=/0/items/StBasilsHymnal1918/StBasilsHymnal1918_jp2.zip&file=StBasilsHymnal1918_jp2%2FStBasilsHymnal1918_0107.jp2&ext=jpg). It repeats the last two lines or so of each verse. If you prefer it without the repeat [I've typed it up here](/pdf/immac/hail-st-basil.pdf)

Rev Fr Hurlbut sets the hymn to the tune that I know well from O Purest of Creatures, based on the German "Maria zu Lieben". [Here is the page from the Internet Archive](https://ia800202.us.archive.org/view_archive.php?archive=/32/items/treasuryofcathol00hurl/treasuryofcathol00hurl_jp2.zip&file=treasuryofcathol00hurl_jp2%2Ftreasuryofcathol00hurl_0230.jp2&ext=jpg). I really like this one, as it is a tune I am really familiar with and closely associate with thinking about Mary.

Finally, Arundel Hymns pairs the text with the tune known as Hanover, which I have heard sung by Maddy Prior in her album of Gallery Hymns of the 18th and early 19th centuries. She sings the tune with "O Worship the King", but [here is the link if you would like to listen](https://www.youtube.com/watch?v=k1DImOORTAw&list=OLAK5uy_kgLL5SUnBjeeoGs2gPn2vM9_X27LJQcLg&index=15). If you'd like the page from Arundel Hymns, [here it is in tiff format](https://archive.org/download/arundelhymnsand00gattgoog/arundelhymnsand00gattgoog_tif.zip/arundelhymnsand00gattgoog_tif%2Farundelhymnsand00gattgoog_0387.tif).






