---
title: Toni Capituli et Orationum
categories:
- General
feature_image: "/images/myorgan600.jpg"
---

I received a question on how to sing some of the Common parts of the Little Office. In particular, the Capitulum or Little Chapter and the Collect or Prayer.

The two parts share similarities, so let's look at them together.

[Tonus Capituli](pdf/tonus-capituli.pdf)

and

[Toni Orationum](pdf/toni-orationum.pdf)

Here is a draft video which I have uploaded to Youtube and Rumble and two much more polished videos which I have already added to the online courses on Udemy and Gumroad.  

<iframe width="560" height="315" src="https://www.youtube.com/embed/QLehOAIZMuk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Further to the Toni Orationem - the Liber Usualis 1962, Liber Antiphonarius 1960 and the Roman Antiphonale 1911 all give two ways - either the Festal/Ferial or the more ancient Solemn/Simple. I've jumped into the more ancient one, as that is what I am more familiar with and it is much more interesting than the Festal/Ferial way. The Festal is similar to the Simple and the Ferial is almost monotone. If you want to look it up, it should be easy if you want to go Festal/Ferial. Then you just need a calendar to see if today is a Feria or not.

Maybe that's confusing.

I'm reminded of a silly saying "Be reasonable, do it my way."

But of course you don't have to do it my way. Maybe I'm wrong. 

The blue books from Baronius give the Festal tone, which is the first one described in the books and it's easy.

Let me know if there's a reason why the Little Office should use the Festal Tone to the exclusion of all others.

God bless you.

