---
title: Marathon part 3
categories:
- General
feature_image: "/images/tertiatwo.jpg"
---

The third installment and the third hour of the day (more or less).

{% include video.html id="zkP-EKXR7HY" %}

So this is the hour that morphed into Mid-Morning Prayer. Being slated for 9am makes it a good one to start the school day. Except that this is a Saturday and my kids are keen to go off to a Garage Sale.

At the end we slip up and give the Monastic or Benedictine response. So, the Roman ending is:

Divinum auxilium maneat semper nobiscum. Amen.

*(May the Divine assistance remain always with us. Amen.)*

and the older Benedictine/Monastic one is:

Divinum auxilium maneat sember nobiscom. Et cum fratribus nostris absentibus. Amen.

*(May the Divine assistance remain always with us. And with our absent brethren. Amen.)*

which is a little closer to what we say in English usually:

May the Divine assistance remain always with us. And may the souls of the faithful departed through the mercy of God rest in peace. Amen.

Which last bit we sort of say earlier in the Hour (Fidelium animae per misericordiam Dei requiescant in pace. Amen.)

Also, I do eventually brush my hair on this day - just in time for Vespers. Singing the Little Office AND keeping a tidy house/brushing my hair/getting schoolwork done/cooking nutritious meals is still beyond me and probably explains why I don't sing the entire Little Office every day.

God bless!

