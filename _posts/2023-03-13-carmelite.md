---
title: Carmelite Chant
layout: post
---


Someone requested details on how to sing the Carmelite Little Office.

Going from [a Carmelite Little Office book from the Internet Archive,](https://archive.org/embed/LatinCarmeliteRiteLittleOfficeOfTheBlessedVirginMary) I've looked up the unfamiliar antiphons on Gregobase and pasted them in here. Even though they're not specifically Carmelite versions, the chant tunes tend to be fairly universal.



[Carmelite Bits](/pdf/carmelite/carmel.pdf)

and in [booklet format](/pdf/carmelite/carmel-book.pdf)

and [here's a recording](https://youtu.be/vJan2i8hKts)


**UPDATE:** From the comments section in the short video, someone provided this transcription from a Carmelite Antiphonal:

```
(c3)VIr(e)go(g) Ma(h)rí(ij)a,(i) *(,) 
non(j) est(k) ti(j)bi(i) sí(j)mi(i)lis(h) or(i)ta(j) in(j) mun(i)do,(i) (;) 
in(i)ter(ivHGF) mu(g)lí(f)e(e)res:(e) (:) 
flo(h)rens(g) ut(h) ro(ij)sa,(i) (;) 
fra(j)grans(k) si(j)cut(i) lí(j)li(ih)um,(g) (:) 
o(h)ra(ij) pro(j) no(i)bis,(i) (;) 
san(i)cta(ivHGF) De(g)i(f) Gé(e)ni(de)trix.(e) (::)
```

I've added it to the [Carmelite Bits PDF](/pdf/carmelite/carmel.pdf) above and here is a peek:


![Virgo Maria, carmelite version](/pdf/carmelite/virgo-maria.png)

