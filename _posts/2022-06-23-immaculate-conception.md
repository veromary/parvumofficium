---
title: Little Office of the Immaculate Conception
layout: post
---

The Little Office of the Immaculate Conception is much shorter than the Little Office of the Blessed Virgin Mary. It has no Psalms, only hymns, which all share the same meter.

**UPDATE**: I've found a really nice 12 page section containing the Little Office of the Immaculate Conception in Latin and English from [The Sodalist's hymnal](https://archive.org/details/sodalistshymnalc00macg_0/page/358/mode/2up), Edwin F MacGonigle, 1887.

<a href="/pdf/officiumparvumImmac.pdf" class="button">12 page PDF</a>

And if your computer doesn't automatically do booklet printing, try [this pdf, flip long side](/pdf/officiumparvumImmac-book.pdf).

[Here is Michael Martin's page with all the prayers in Latin and English](https://www.preces-latinae.org/thesaurus/BVM/OPConImm.html)

**UPDATE:** I've transcribed a chant tune as sung in [this video](https://www.youtube.com/watch?v=QLisXhdQfV8)

<a href="/pdf/immac/chant-tune.pdf" class="button">Chant tune, 5 page PDF</a>

Formerly, looking to sing it in Latin, I might use this tune:

[Gregobase closest hymn (sequence)](https://gregobase.selapa.net/chant.php?id=12064)

That tune has three variations over 6 verses whereas the Little Office of the Immaculate Conception only has two or four verses at a time. I might arrange the variations A B C A, or just use the first variation.

Looking in J M Neale's book The Hymnal Noted, there are some Latin hymns with a similar meter: *Ades, Pater supreme* and *Cultor Dei memento*. It also adapts a version of *Ave maris stella*, which could open up many more tunes.

The Brebeuf Hymnal pairs Salve mundi with 5 different tunes, most notably the tune for Good King Wenceslaus!

If I were to sing it in the metrical English version, I would use the Lourdes Hymn Tune or "O Purest of Creatures" *(Maria zu lieben)*. The Welsh tune St Denio, from "Immortal Invisible", would be great too.

[English literal translation](https://archive.org/details/TheLittleOfficeOf/)

So, this article is a bit disjointed now with the updates. Maybe it's time for a separate section!

Listen to our renditions in the [Youtube Playlist](https://www.youtube.com/watch?v=OVx-wBoDUWo&list=PLR7cOUX2gl7rFRShjbwRMgC1qCbqrtokf)

