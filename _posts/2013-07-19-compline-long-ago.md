---
title: Compline from Long Ago
categories:
- General
feature_image: "/images/myorgan600.jpg"
---

Back when I used a Wordpress blog, I uploaded some recordings of my family singing Compline from the Little Office of the Blessed Virgin Mary.

You can read the original post [here](https://hymni.wordpress.com/2013/07/19/little-hymn-for-the-little-hours-of-the-little-office/)



