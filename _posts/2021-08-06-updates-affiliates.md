---
title: Updates and Affiliates
layout: post
feature_image: "/images/littleoffice.jpg"
---

After updating [Brandt Lab](https://brandt.id.au) and then [Kids Chant](https://kidschant.com) I turned to the Little Office, and have updated the booklets again.

The updates aren't huge &mdash; the translations for each psalm verse are back on their own line, which increases the number of pages, but the page width is a little bigger, so that helps a little.

The feast of the Assumption of the Blessed Virgin Mary is coming up, so that gives me more reason to keep refining these booklets and videos.

Also, Gumroad gives the ability to offer affiliate kickbacks. I'd love to offer these to people who promote my course. Because of the Udemy contract, I have to charge more on Gumroad, and fewer people are buying the course through there, which makes sense, but I think the Gumroad platform gives a better deal, being able to download full resolution videos and sending out less advertising for other courses.

I have set the affiliate cut at 33%, so you can get about AU$8 for each person who signs up using your link. Gumroad asks you how you plan to promote the course. Right now this is new to me, so I'm sure whatever you write there will make me happy to have you as an affiliate. Gumroad gets me to approve any new affiliates, so you might be waiting a day or two for me to get back to you. There are no minimum requirements, so [sign up here](https://gumroad.com/veromarybrrr/affiliates).


