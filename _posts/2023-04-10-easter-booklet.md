---
title: Easter Booklet
layout: post
---

I'm heading away for a few days and wanted to take the Little Office with me. I've made this little booklet, taking out some parts that I have reliably in memory.

[easter.pdf](/lo/easter.pdf)

I place it here in case it's useful to someone.

I've also started adding in the extra bits from the 1910 version of the Little Office - the commemoration of Saints being the biggest bit, plus the Kyrie Eleisons.

Happy Easter!!!

Christus resurrexit!

Surrexit vere! Alleluia!

