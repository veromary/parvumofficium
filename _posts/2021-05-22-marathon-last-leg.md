---
title: Last leg of the Marathon
categories:
- General
feature_image: "/images/vespersone.jpg"
---

I'll bundle up the last three videos into this one last post. I sing these ones by myself, so they're not nearly so interesting.

Here's Sext and None together:

{% include video.html id="6tyAeW1JaJ8" %}

Here's Vespers:

{% include video.html id="EPqikDQVmDw" %}

(or should it be Here are Vespers?)

And finally Compline!

{% include video.html id="RV7beT8Rjg0" %}

Thank you to everyone making this not only possible, but egging me on to record and publish things like this. Thank you for all your prayers for my family and me!

Finally, this isn't a normal day in the life of my family. It's an illustration of the time taken to sing the Little Office. I think some people don't realise that the prayers and psalms take so long. And the Divine Office is even longer - so be especially kind to any priests you know who pray the whole Office. Sometimes we expect so much from priests.

That said, it is possible, and there is a lot of food for meditation in there.

