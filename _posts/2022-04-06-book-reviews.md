---
title: Book Reviews!
layout: post
feature_image: "/images/fourLittleBooks.jpg"
---

I will be reviewing four copies of the Little Office which are currently in print:

![Baronius](/images/book-baronius.jpg)
![Angelus](/images/books-angelus.jpg)
![Bonaventure](/images/books-bonaventure.jpg)
![My reprint](/images/books-mine.jpg)

From left to right: Baronius Press, Angelus Press, Bonaventure Publications, and my cheap reprint.

### Vital Statistics

|---|---|---|---|---|---|
|Book | Height | Width | Thickness | Letter Height | Baselineskip |
|----|----|----|-----|----|----|
| Baronius    | 162mm | 106mm | 15.8mm | 2.2mm | 3.8mm |
| Angelus     | 152mm | 102mm | 11.6mm | 2.4mm | 4.2mm |
| Bonaventure | 173mm | 120mm | 14.5mm | 2.0mm | 2.7mm |
| My reprint  | 176mm | 108mm | 10.1mm | 3.3mm | 4.8mm |
|----|---|---|---|---|---|

### Prices

* Baronius Press: USD 29.95 - [baronius.com](https://www.baroniuspress.com/book.php?wid=56&bid=47#tab=tab-1)
* Angelus Press: USD 19.95 - [angeluspress.org](https://angeluspress.org/products/little-office-of-the-blessed-virgin-mary)
* Bonaventure Publications: USD 24 - [libers.com](http://www.libers.com/lof.htm)
* My cheap 1832 reprint: USD 18 - [shop.jubil.us](https://shop.jubil.us/product/officium-parvum-beatae-virginis-mariae-with-english-translation-from-1832/) or [lulu.com](https://www.lulu.com/en/us/shop/richard-grace-and-veronica-brandt/officium-parvum-beatae-virginis-mariae-with-english-translation-from-1832/paperback/product-egjvr4.html?page=1&pageSize=4)
* (Another option: the Breviarium Romanum: USD 13.48 - [lulu.com](https://www.lulu.com/shop/-catholic-church-and-david-siefker/the-day-hours-of-the-1962-roman-breviary-breviarium-romanum-diurnale/paperback/product-145jmp5v.html?page=1&pageSize=4) )


I live in Australia and sometimes postage from the US can be steep, so I use Planet Express to cut down on the shipping prices. Delivering the Angelus Press book from Planet Express's depot in California took under a month and cost USD 13.69. If you'd like to try them out, [this is my affiliate link](https://planetexpress.com/?ref=10123).

## More Details:

### Baronius Press

About a decade ago, I decided to buy copies of the Little Office for my family. I bought the Baronius Press version as it was connected to the FSSP and included some chant notation. The shop, Aquinas and More, offered to add gold lettering to the covers for a small surcharge, so we had six copies with each person's initials on the front. Plus one extra copy just in case.

The Baronius Press editions are the most expensive and also the most fancy, with two ribbons and gilt edges. Unfortunately, they haven't withstood the test of time as well as I could have hoped. It should be added that they were in regular use by children, so maybe this is to be expected. The ribbons have cut into the pages and the marbled endpapers have ripped.

My main disappointment was that they didn't contain all the notation needed to sing the Little Office. The psalms lack accent marks and asterisks, even leaving aside any description of how to sing the psalm tones apart from the cryptic EUOUAEs.

The translations of Scripture are odd too. Maybe this is the Douay Rheims before the Challoner revision? They're lovely, but a bit unique.

The columns really mess up the hymns. The editors have tried to ameliorate this by shrinking the type for the hymns, but this brings more strain on the eyes, especially when hymns are such a fun part of the office. I guess you are likely to memorize the hymns earlier than other parts, so maybe they thought you wouldn't need to read them too many times.

Another funny thing is that the English comes first - being in the left column. I have typeset booklets for some very fussy people and one of these in particular stipulated that the Latin should always come first. It should either be on the inside column, in order to afford more protection, or if that is not possible, then in the first column. This book doesn't follow this rule, putting the English first on the left, followed by Latin on the right column of each page.

However there are lots of good things about the Baronius Press edition. The copious appendices give lots of information about the Little Office and rubrics involved in its regular recitation. The Litany of Our Lady is included, in Latin and English.

This one is the only one of the four to give separate sections for Office 1, Office 2 and Office 3. Office 2 describes variations for Advent and the feast of the Annunciation. Office 3 covers Christmas through to the Purification (2 Feb). The complete office is duplicated for the major hours, but only the antiphons given for the minor hours with cross references to Office 1 for the things that stay the same.


### Angelus Press

This one is my newest one, so I have not had a chance to see how it holds up.

I am very pleasantly surprised how easy this one is on the eye. There are no columns - the left page carries the English and the right page the Latin, having more relaxed long lines. The Latin has the accent marks and asterisks, making it easier to chant the psalms. Like the Baronius, it puts the English first, being on the left hand page. I'm not sure if it could be argued that being the *verso* page, this could be seen as a position of lesser importance. I wish I had my fussy person to refer to. May he rest in peace.

The book is structured to run through the Little Office from Matins to Compline with variations for the seasons included inline.

The Bible passages are straight from the Douay Rheims, Challoner version of the Bible. This is a bit more consistent than the other three.

It also includes the Office of the Dead: Matins, Lauds and Vespers.


### Bonaventure Press

Bonaventure Press has reprinted the Little Office from a larger book from 1904. They have changed the page numbers, but some of the cross references still point to pages in the old numbering, so you might like to fix those up with a pencil when you come across them.

Being from 1904, it follows an older arrangement, with extra psalms in Lauds and extra *Kyrie eleison*s here and there. It is fairly similar though.

The psalms have asterisks, but no accent marks.

The translations are very close to the Douay Rheims, Challoner Bible. Small variations, though they wouldn't be too much of an issue.

Being in two columns, the hymns get messy with their broken lines. This is compensated for by beginning each verse with an indented line. The rubrics are in italics in both Latin and English in their respective columns.

The Latin comes first, being in the left column throughout.

This is the only one of the four books here with a rigid hard cover.


### My Reprint

I reprinted a little Irish book from [the internet archive](https://archive.org/details/officiumparvumbe00cath/page/n5/mode/2up). I've done a little tidying up of the scans, but it's still a bit rough. Curiously some accent marks are backwards - I still can't tell which are graves and acutes, but looks like maybe they were short of the right sort in their box of type. I've even come across some words featuring multiple accent marks.

With regard to content, it is very much like the Bonaventure Press book, despite being about 70 years earlier. It has the extra psalms for Lauds and the *Kyrie eleisons*.

The Bible texts are rather unique. I don't know whether these ones are from the original Douay Rheims, but they don't match the Baronius version either. There are some archaic words like "ingulphed" as a translation for *absorbuisset*.

Being another two column book, the hymns get messy, but this is compensated for by beginning each verse with a word in small caps. The rubrics are generally in italics in English spanning both columns.

The Latin comes first, being in the left column throughout.

The book also includes Graces Before and After Meals in Latin and English. There is a booklet available freely on the internet which gives the chant tunes for these prayers: [benedictiones.pdf](https://gregorio-project.github.io/examples/benedictiones/benedictiones.pdf). They match up pretty well with this book, though the Little Office book has a few more prayers.

## Conclusions

While none of them are completely perfect, the Angelus Press copy is very good. Having the prayers laid out on whole pages without columns is very beautiful.

Angelus and Baronius both employ red lettering, while the Bonaventure and mine are monotone facsimile copies.

For readability, the Angelus and my reprint come out ahead with larger lettering. My reprint suffers a bit with the two column layout and some slipshod typesetting, plus some damage to the original copy before it was scanned.

Having the chant in the Baronius does set it apart, but you still need extra chant in order to sing all the office. Printing chant booklets from [this webpage](/) for singing purposes is still a good idea. Proper bound copies are good for carrying with you. I think most people would just read through the prayers when they are out and about. Singing would usually be a more premeditated thing. Then again, singing the Little Office as you walk around the supermarket would be pretty amazing. But for that you would need to have memorized it. Which is totally doable.



