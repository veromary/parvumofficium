---
title: A Treatise
layout: post
---

![Me reading the back of the book](/images/cover-treatise.jpg)

[The Scans on the Internet Archive](https://archive.org/details/littleofficeofou00taun)

[The Printed Book](https://shop.jubil.us/product/the-little-office-of-our-lady-a-treatise/)

[The Epub](/pdf/LittleOfficeATreatise-EthelredTaunton.epub)

[The Video](https://youtu.be/xYzZlG4tMkA)


### Other Books

My earlier article on the [The Dominican Little Office](/2022/03/04/dominican-little-office/)


Here is my copy of [**The Divine Office: Explanation of the Psalms and Canticles, by St Alphonsus Liguori**](https://www.lulu.com/shop/david-siefker/commentary-of-the-psalms-liguori/paperback/product-1kvqd9nq.html)

The scans are still [on the internet archive](https://archive.org/details/alphonsusworks14liguuoft) - it's volume 14 of the Complete Works of St Alphonsus Liguori.

[This book from Barnes & Noble](https://www.barnesandnoble.com/w/explanation-of-the-psalms-canticles-in-the-divine-office-st-alphonsus-m-liguori/1028502223) seems to be the same book.

There are other reprints available on other popular online bookshops.

