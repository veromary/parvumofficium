---
title: Missing Piece of Vespers
categories:
- General
feature_image: "/images/midday.jpg"
---

Someone asked for updated straight through recordings now that Paschaltide is behind us. In the morning I set up in the living room and recorded Matins and Lauds, Prime, Terce and Sext. In the afternoon I set up in the spare room and made some more recordings using a different camera which I haven't used much before, covering psalmtones for modes 8, 2 and 4. When I started the Canticle for Compline, the camera stopped, the memory card was full. I took the card back to my computer and had a look at the recordings only to find they were all out of focus.

Anyway, while I was singing Vespers, I noticed there was no Versicle and Response. This tiny little part crops up in all the hours, including Vespers. I had just forgotten to put it in.

This is why it's a good idea to get an official copy of the Little Office - my copies are not authorised and not 100% reliable. I've put them together to help explain how to sing the hours, and I hope they help. It's going to be a while though before they are complete.

I've updated the vespers booklet. I think the easiest way to update the Udemy course is to put the Versicle in the Intro to the Magnificat Lesson.

Another weird thing that I noticed when editing the new video for the Psalm Tones was the number of Psalms in the Little Office.

There are thirty three psalms.

**33**

Now, I'm fond of numbers. I just recently stumbled across the fact that 3x7x37=777 and want to make that into a T-shirt. But I'm not the only one who likes the number 33.

Our Lord lived on earth 33 years.

Traditional cassocks or soutanes like to have 33 buttons down the front.

I have a hazy memory that there was a specific number of signs of the cross or genuflections in the Latin Mass, but can't seem to verify that here.

It turns out the old Divine Office planned 33 psalms a day. Now, 33 x 7 > 150, but there was a bit of repetition of the more prominent psalms. Also some of the longer psalms (eg. 118) were broken up into sections to make them more manageable. But the Little Office doesn't have any broken up psalms.

So, in mastering the Little Office of the Blessed Virgin Mary, you assimilate thirty three psalms into your memory. Plus four canticles. And a few hymns. And a whole lot of antiphons.

And if you would like me to sing them all for you, here are two options for you:

<center>
{% include button.html text="via Gumroad" link="https://gum.co/bvmsing" icon="gumroad" color="#36A9AE" %}
&nbsp; or &nbsp;
{% include button.html text="via Udemy" link="https://www.udemy.com/course/sing-officium-parvum-bmv/?referralCode=7C9068C18692B9588F76" icon="udemy" color="#EC5252" %}
</center>

I hope it helps!

