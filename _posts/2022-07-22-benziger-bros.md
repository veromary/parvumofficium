---
title: Benziger Brothers 1915
layout: post
---

![New Book Cover](/images/cover-sq.jpg)

[The Little Office, Benziger Bros 1915](https://shop.jubil.us/product/the-little-office-of-the-blessed-virgin-mary/)

I found a new favourite scan of the Little Office of the Blessed Virgin Mary [here on the Internet Archive](https://archive.org/details/littleofficeofbl00newy/page/n7/mode/2up) I've tidied it up and set up a reprint via Lulu.

The original includes the Office of the Dead plus a section of Commemorations, but when I first printed the book including these it felt quite bulky and so I took them out for subsequent versions. Maybe I should make both available.

The really wonderful things about this book:

* Very similar to the "current" books, ie Baronius Press and Angelus Press as there were changes in 1910 which took out some small psalms in Lauds. If you want those psalms, you'll need an older copy, like [my earlier 1832 reprint](https://shop.jubil.us/product/officium-parvum-beatae-virginis-mariae-with-english-translation-from-1832/) or the Bonaventure Press reprint. Still has those little Kyrie Eleisons though.
* Nice big print, whole page layout, drop caps.
* Accent marks, asterisks to mark the mediant of each psalm verse.
* Handy introduction with rubrics and ceremonial.

I also discovered the the [British Library collection of Illuminated Manuscripts](https://www.bl.uk/catalogues/illuminatedmanuscripts/welcome.htm) have released images into the  [Public Domain](https://www.bl.uk/help/how-to-reuse-images-of-unpublished-manuscripts). So I am very much looking forward to reusing these images. All the Books of Hours seem to be predominantly copies of the Little Office of the Blessed Virgin Mary! It is wonderful how recognizable they are!

