---
title: Old Books
layout: post
---

[A 1740 copy](https://archive.org/details/BeataeMariaeVirginisOfficiumV1/)
Latin with Italian rubrics. Includes the Office of the Dead and the Gradual Psalms and the Office of the Most Holy Sacrament. Lots of prayers in Italian at the end, spelling Jesus as Giesu instead of the standard Italian today using Gesu.

[A 1915 copy](https://archive.org/details/littleofficeofbl00newy/) in Latin and English also with the Office of the Dead.

[Officium Parvum from 1832](https://archive.org/details/officiumparvumbe00cath/) in Latin and English - rather nice book, with advantages over the Baronius Press version, including accent marks and the asterisks to mark the mediant in each psalm verse. Plus graces before and after meals.

[Officium Parvum from 1866](https://archive.org/details/officiumparvums00deadgoog/) in Latin and German (printed in Munich, so pretty sure it's German). Lots of other prayers in there too. The German is in a Fraktur script.

[Officium Parvum from 1757](https://archive.org/details/officium-beatae-mariae-virginis-s-pii-v/) in Latin with Italian rubrics. Plus the Office of the Dead and lots of stuff.

[1693](https://archive.org/details/bub_gb_CbmD5Sn8vTUC/) Jesuit book, poetic paraphrases? Confusing but looks interesting.

[1790](https://archive.org/details/gri_33125011169139/) very good scans, with rubricated initials. 

