---
title: Passiontide
layout: post
---

A quick note to say the in Passiontide in the Little Office the Gloria Patri is nowhere omitted.

That means we keep singing Gloria Patri in the Little Office, even though in the 1962 Divine Office and the Mass we leave that prayer out for Passiontide.

Also, during the Sacred Triduum (Holy Thursday, Good Friday, Holy Saturday) the Little Office is not said publicly. I will look into making a resource outlining how to sing the Divine Office for this time. Much of the Divine Office is stripped right back, making it much more approachable.

![Our statues and crucifix covered with purple cloth](/images/purplecloth600.jpg)

It's also customary to cover images of Jesus, Mary and the saints with purple cloth on Passion Sunday, which was the Sunday before Palm Sunday in the old calendar. The purple comes off the crucifixes after the 3pm Good Friday Service and the rest after the Resurrection.
