---
layout: post
title: Dominican Little Office of the Blessed Virgin
---

**UPDATE:** The Internet Archive has [this booklet](https://archive.org/details/Officium_Beatae_Mariae_Virginis_pro_cantu_editum_secundum_ritum_Ordinis_Praedi/page/n61/mode/2up) with all the music for the Dominican Little Office dated 1928. I've cleaned it up with scantailor for you [to download and print here](/pdf/officium_parvum_bvm_op.pdf) - just updated 2022-07-29 as the first version was missing pages 2-11.

**Another UPDATE:** [Video singing Matins and Lauds](https://www.youtube.com/watch?v=0BmDN3v9pEk)

**Yet Another UPDATE:** [Printed Version available](https://shop.jubil.us/product/officium-beatae-mariae-virginis-pro-cantu-editum/) and a [Playlist with More Videos](https://www.youtube.com/playlist?list=PLR7cOUX2gl7rDr0ahnDJZopKgfYi5ss9H)

I like getting questions about the Little Office. Someone asked me about making supplementary videos describing the differences with the Dominican Little Office, as I had only covered the Roman Little Office so far.

I came across a little detail about St Dominic, which you'll like. He would sing the Office, rather than save time and just read it. Which makes me feel that all this extra work figuring out how to sing the Little Office might not be entirely superfluous.

Keller Book was a wonderful webpage collecting tables comparing different Office books. Unfortunately the original webpage is broken, but it has been mirrored by [gregorianbooks.com](https://gregorianbooks.com)

[Here is their link comparing the Little Offices](http://www.gregorianbooks.com/gregorian/www/www.kellerbook.com/PARVPS~1.HTM). I'll paste the table here to ensure it doesn't fall off the internet again:

{:class="table-striped"}
|----|----|----|----|---|
| **&mdash;**   | **Roman**  | **Carmelite**   | **Dominican**     | **Monastic**      | **Amplior**       |
|----------|------------------------------------------------------------------------------------------|-----------------|-------------------|-------------------|-------------------|
| Matins   | Sunday,<br> Monday,<br> Thursday.<br> 8, 18, 23 <br> Tuesday,<br> Friday.<br> 44, 45, 86<br> Wednesday,<br> Saturday<br> 95, 96, 97 | (same as Roman) | Every day<br> 8, 18, 23 | (same as Roman)   | (same as Roman)   |
| Lauds    | 92 <br> 99 <br> 62<br>Dan 3<br>148 | (same as Roman) | (same as Roman)   | (same as Roman)   | (same as Roman)   |
| Prime  | 53<br>84<br>116<br> | 53<br>116<br>117<br> | 119<br>120<Br>121<Br> | (same as Roman) | (same as Roman) |
| Terce    | 119<br>120<br>121 | (same as Roman) | 122<br>123<br>124 | (same as Roman)   | (same as Roman)   |
| Sext     | 122<br>123<Br>124  | (same as Roman) | 125<br>126<Br>127       | (same as Roman)   | (same as Roman)   |
| None     | 125<Br>126<Br>127  | (same as Roman) | 128<br>129<Br>130       | (same as Roman)   | (same as Roman)   |
| Vespers  | 109<Br>112<br>121<Br>126<Br>147 | (same as Roman) | (same as Roman)   | (same as Roman)   | (same as Roman)   |
| Compline | 128<br>129<Br>130 | 12<br>42<Br>128<Br>130   | 131<Br>132<Br>133 | (same as Roman)   | (same as Roman)   |

You can see the Carmelite is more similar to the Roman. The Dominican is quite different. I like how Vespers and Lauds are the same.

But it's not just a matter of which psalms are sung at which hour. I've sung with Benedictines on a few occasions and there are lots of little differences all over the place - from the way they sing *Deus in adjutorium meum intende* right through to little differences in the tunes for the Marian Antiphons. Although I have less experience with Dominicans, it looks like it's a very similar case.

### Where to find the music

The instructions for how to sing the Roman Little Office are found in the Antiphonale Romanum. The equivalent book in the Dominican world is the Antiphonarium 1933, Gillet, available to download [from the Musica Sacra Website](https://musicasacra.com/miscellany/dominican-liturgy/).

The relevant pages would be:

* p 1 has the tone for the *Deus in adjutorium meum intende*
* p 5 has the tones for the versicles - more subtle differences
* p 1*-15* for feasts of the Blessed Virgin Mary
* p 130*-139* for the Office of Our Lady on Saturday

### Getting Copies of the Dominican Little Office

New Liturgical Movement has an article about a [Dominican Prayer Book](https://www.newliturgicalmovement.org/2018/11/dominican-little-office-of-bvm-and.html) back in print [via Lulu](https://www.lulu.com/en/us/shop/order-of-preachers/dominican-prayer-book-1962/paperback/product-12qpvpm5.html). 
It has some pages missing (pp 166-265, 525-89), but as the other books I've found are of uncertain copyright status, it should be a good way to get a physical copy.

It may be similar to [this scanned book](https://archive.org/details/OfficeBookForDominicanSisters/page/n7/mode/2up) from 1941. This one also has missing pages (pp 113-130)

[This page](https://willingshepherds.org/Office%20of%20the%20Dead.html#LITTLE%20OFFICE) gives an English version of the Dominican Little Office.


