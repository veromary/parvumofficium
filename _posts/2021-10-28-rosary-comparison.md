---
title: Little Office vs Rosary
date: 2021-10-28
layout: post
feature_image: "/images/lo-vs-rosary.jpg"
---

Hello, today I’m looking at the difference between the Little Office of the Blessed Virgin Mary and the Holy Rosary.

They both go back at least to Medieval times. They both have roots in the Divine Office or Liturgy of the Hours. This is a system of prayer, often prayed together, consisting of Psalms, Hymns and Spiritual Canticles – like St Paul says to the Colossians: 3:16 Let the word of Christ dwell in you abundantly: in all wisdom, teaching and admonishing one another in psalms, hymns and spiritual canticles, singing in grace in your hearts to God.
The Jews already had customs of praying the Psalms every day and this continued into Christian practice. St Benedict arranged the Psalms into eight hours prayed each day over a seven day week, not setting this on concrete, but explicitly permitting adaptations. And adaptations came over time.
The Little Office of Our Lady is an adaptation, reducing the 150 Psalms down to just 33, which is also the number of Psalms which used to be prayed in the course of one day, and the number of years Jesus spent on Earth in the Flesh.

Passing on all these words depended on learning these by repetition or by reading and there were lots of people who couldn’t read. There grew up a practice of praying on beads. The Orthodox still have a string of 150 knots or beads called a Chotky, one bead for each of the 150 Psalms. They commonly pray a short prayer, the Jesus Prayer, on each bead. Jesus, Son of God, have mercy on me, a sinner. It’s simple, meditative.

Around AD 1200, St Dominic promoted a variation on this called the Rosary, involving 50 Ave Marias or Hail Marys.

The Little Office is sort of part of the Divine Office. You find the chant for the Office of Our Lady in the Antiphonale Romanum, which is the same book where you find the music for the Divine Office. There are lots of similarities with the Saturday Office of the Blessed Virgin Mary.

### Time Taken

Praying the entire Little Office privately, just reading the words, you might be able to get done in an hour each day. Singing will take about 2 hours in total. But you don’t pray it all at once and you don’t have to pray the whole thing. Singing Prime in the morning will take about 7 minutes. Saying it could be less than 5 minutes. Singing one of the major hours – Matins, Lauds or Vespers, - would take about ½ hour.

Praying the Rosary privately, just saying the words quietly, takes about 15 minutes for the usual 5 decades. Some people just say a decade, which would be done in less than 5 minutes. St Louis de Montfort encouraged people to take on the full 15 decades, which would take ¾ hour. Saying a Rosary together with some reflection on the mysteries each decade and singing some parts can take half an hour for 5 decades. The Gregorian Chant Rosaries which you see on youtube take about an hour for 5 decades. Padre Pio was known to just keep saying Rosaries one after another.

So, either way, the time taken to pray the Rosary or the Little Office could range from 5 minutes, to ½ an hour, a full hour, or even two hours.

### Efficacy

I thought I would add this in, but really I am being a bit cheeky here. The efficacy of Christian Prayer has less to do with the prayer and more to do with the charity with which it is prayed, especially uniting our poor hearts to God’s infinite Love. We pray for the Holy Spirit to inspire us and it boils down to increasing God’s love living in our souls through Grace.

Until someone invents a supernatural Charity meter, then it’s just God who can measure the efficacy of our prayer.

### Indulgences

An explanation on the meaning of Indulgences as granted by the Catholic Church is beyond the scope of this short video.

The rules on Indulgences have been greatly simplified since the 1960s. From my reading of the 4th Edition of the Enchiridion of Indulgences, 1999, there is a Partial Indulgence attached to both the Little Office of the Blessed Virgin Mary and the Rosary. There is a Plenary Indulgence attached to saying the Rosary in an Oratory or a Church or as a family or religious community, under the usual conditions.

So the Rosary comes out ahead where Indulgences are concerned.

### Learning Curve

This is probably the biggest difference. I think just about anyone could get the hang of the Rosary pretty quickly. The Little Office, even just saying it in English, involves ideas about which hours to say when. Once you get past that hurdle, then it could just be a case of reading through the relevant section in your book.


### Access to Materials

There’s a good chance you can find a bazillion Rosary leaflets in the foyer or narthex of your local Catholic Church. If your local Catholics aren’t so enthusiastic about promoting the Rosary, then you can print your own pamphlets from the Internet or order some online. Likewise, Rosary Beads are fairly easy to find and you can make your own. Getting them blessed by a priest is highly recommended but not essential.

Many people also fall back on their own ten fingers to pray the Rosary.

To pray the Little Office, you can buy fairly expensive physical copies. If that’s a bit much for you just yet, there are websites like divinumofficium.com and lobvm.com which give the text of the prayers in a convenient webpage. My website littleoffice.brandt.id.au gives more information on singing the Little Office, plus pdf booklets you can print out. You can start by reading the prayers and then add in music as you like.

Praying from a printed copy generally looks better than praying from your phone.

It is possible to memorise the whole Little Office, but it does take a lot longer than memorising the prayers of the Rosary. Settling on one of the hours each day is a good way to start. It’s like planting a garden of wonderful prayers in your memory which you can keep with you whereever you go. Both the Rosary and the Office are great to fill your memory with.

### Variety

#### Unique words:

* Rosary: 181 unique words out of 2977 words, (6.1%)
* Matins: 791 unique words of 4,114 total (19.2%) (including all 3 nocturns)
* Lauds: 478 unique words of 1,951 total (24.5%)
* Prime: 255 unique words of 761 total (33.51%)
* Terce: 275 unique words of 772 total (35.62%)
* Sext: 257 unique words of 734 total (35.01%)
* None: 273 unique words of 727 total (37.55%)
* Vespers: 454 unique words of 1,582 total (28.7%)
* Compline: 331 unique words of 1,071 total (30.91%)

### Summary:

* Time Taken: Tie
* Efficacy: God knows
* Indulgences: Rosary wins
* Learning Curve: Rosary wins
* Access to Materials: Rosary wins
* Variety: Little Office

