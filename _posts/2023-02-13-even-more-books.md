---
title: Even More Books
layout: post
feature_image: "/images/evenmorebooks.jpg"
---


### Previous articles:

[Book Reviews](/2022/04/06/book-reviews/) - the first article detailing the first four books.

[Benziger Brothers 1915](/2022/07/22/benziger-bros/) - the second article, discussing my second batch of reprints.

### New Books:

[My Paperback including Office of the Dead](https://shop.jubil.us/product/little-office-of-the-blessed-virgin-mary-with-office-of-the-dead/)

[My Hardback, without Office of the Dead](https://shop.jubil.us/product/hardback-little-office-of-the-blessed-virgin-mary-1915/)

[Karl Keller's Little Office big Paperback](https://www.lulu.com/shop/karl-keller/little-office-of-the-blessed-virgin-mary-latinenglish-paperback/paperback/product-14r54dqv.html?q=&page=1&pageSize=4)

and [Christopher Kent's book](https://www.amazon.com/Traditional-Little-Office-Blessed-Virgin/dp/B0B4QNL1TW/) which I haven't linked to here on the blog before


