---
title: Marathon part 2
categories:
- General
feature_image: "/images/prime-three.jpg"
---

The next installment of the Little Office Marathon from last Saturday is the hour of Prime - named as the first hour of the day, so at sunrise.

{% include video.html id="UD0wypzEMeM" %}

I mention the Roman Martyrology at the end - that's a feature of  Prime from the full Divine Office. It's a bit macabre at times, but really interesting. You can read more about it at another webpage of mine:
[roman-martyrology.brandt.id.au](https://roman-martyrology.brandt.id.au)

We have a rule that breakfast comes after Prime, so that may explain the better attendance here.

This is the one hour that we can more or less recite from memory. Our younger members are still learning it. For a long time they would not join in, and fair enough. Then they would pick up a bit here and there. Learning to read really speeds things up. Reading Latin is great for phonics skills. Picking up the meaning is the next step. I really need to get the translations into the booklets. Maybe have a version without translations too for people for whom the English is not so helpful.

God bless!

