---
title: Sacred Triduum
layout: post
---

The Little Office is not sung in public during the Sacred Triduum. The last Little Office hour would be Compline on Wednesday and would resume with Matins of Sunday (with the Te Deum!)

Singing the Roman Office is not too tricky for this time. It is all said, not sung, except for Matins and Lauds, which is known as Tenebrae or Darkness. You can find many recordings of Tenebrae for each of the days here:

[Tenebrae at Sensus Fidelium](https://www.youtube.com/@SensusFidelium/search?query=Office%20of%20Tenebrae)

Watch out for Christopher Jasper from the [Gregorian Chant Academy](https://gregorianchantacademy.com/) in the videos from St Joan of Arc Church, Idaho.

[Talks about the Triduum at Sensus Fidelium](https://www.youtube.com/@SensusFidelium/search?query=triduum)

[Office des Tenebres](https://www.youtube.com/@itemissaestparis/search?query=Tenebres) a French place with very beautiful Tenebrae Offices with Polyphony - Jeudi is Thursday, Vendredi is Friday and Samedi is Saturday.


