---
title: Chaplet of St Michael
layout: post
categories:
- Prayers
feature_image: "/images/myorgan600.jpg"
---

The Latin Prayer Podcast posted [the Chaplet of St Michael Archangel in Latin](https://www.podbean.com/media/share/pb-s2bav-a04cc9)

As a fusspot, I wanted to print a pretty copy.

So, here it should be:

* [A5, 4 pages](/pdf/chapletMichael.pdf)
* [A4 booklet](/pdf/chapletMichael-book.pdf)

And here's [the Matt Fradd interview with an exorcist](https://www.youtube.com/watch?v=x6YTz-B24FA) which prompted me to pursue this.

