---
title: About
aside: true
---

The Little Office of the Blessed Virgin Mary is a scaled down version of the Divine Office with a focus on Mary, the Mother of God.

While this is often carried out as a private practice, simply reading through the texts quietly to oneself, the liturgical books do carry information on singing the Office as you would any of the Divine Office.

In practice, you might combine the too - praying it quietly most hours and choosing a few to sing together in common. You could also sing particular parts as you pray quietly. You could learn some of the sung parts to sing during your day.

The Little Office has been highly recommended over the centuries, for supplementing the Divine Office or substituting for it where time and/or money has been an issue. It is a beautiful glimpse into the rich tradition of singing the Psalms, probably carried down from the Jewish Temple liturgies.

### Printed Editions

* [The First Four](/2022/04/06/book-reviews/)
* [Benziger Brothers](/2022/07/22/benziger-bros/)


