---
title: Podcast
---


A work in progress - the idea is to make all the variations available as podcast episodes for you to download on your phone and pray along to.

<center>
{% include button.html text="Little Office Podcast at Substack" link="https://littleoffice.substack.com" icon="substack" color="rgb( 255, 103, 25 )" %}
</center>

The plan is to begin each title for each hour with a short code to help you find the right recording. I estimate there will be over 60 recordings required to cover the hours.

The first letter indicates the hour: **M, L, P, T, S, N, V, C** - that should be fairly self-explanatory. 
Modifications for Advent, Christmas and Paschaltide are given with the letters **A**, **N** (for Nativitas) and **P**.
Modifications for Septuagesima (Laus tibi Domine and no Alleluias) is indicated by the number **70**.
Matins takes a few more variables. **1** = Sunday, Monday, Thursday; **2** = Tuesday, Friday; **3** = Wednesday, Saturday. **TD** indicates the Te Deum.

I'll upload the Marian Antiphons separately.

Update: **May 2023** I've moved the podcast over to [Substack](https://littleoffice.substack.com) and still figuring out the details.

Let's see if I can clarify the code idea:

### Everyday, default settings:

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3**, **M2/3-N1**, **M3/3-TD** |
|    | Tues, Fri     | **M1/3**, **M2/3-N2**, **M3/3-TD** |
|    | Wed, Sat  | **M1/3**, **M2/3-N3**, **M3/3-TD** |
| Lauds |  | **L** |
| Prime |  | **P** |
| Terce |  | **T** |
| Sext |  | **S** |
| None |  | **N** |
| Vespers |  | **V** |
| Compline |  | **C**, Salve Regina |

### Advent 

except for Immaculate Conception (and Our Lady of Guadalupe if you recognise this feast):

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3**, **M2/3-N1**, **M3/3-A**  |
|    | Tues, Fri     | **M1/3**, **M2-3-N2**, **M3/3-A** |
|    | Wed, Sat  | **M1/3**, **M2/3-N3-A**, **M3/3-A** |
| Lauds |  | **L-A** |
| Prime |  | **P-A** |
| Terce |  | **T-A** |
| Sext |  | **S-A** |
| None |  | **N-A** |
| Vespers |  | **V-A** |
| Compline |  | **C-A**, Alma Redemptoris in Advent |



### Immaculate Conception (and Our Lady of Guadalupe if you recognise this feast):

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3**, **M2/3-N1**, **M3/3-A-TD**  |
|    | Tues, Fri     | **M1/3**, **M2-3-N2**, **M3/3-A-TD** |
|    | Wed, Sat  | **M1/3**, **M2/3-N3-A**, **M3/3-A-TD** |
| Lauds |  | **L-A** |
| Prime |  | **P-A** |
| Terce |  | **T-A** |
| Sext |  | **S-A** |
| None |  | **N-A** |
| Vespers |  | **V-A** |
| Compline |  | **C-A**, Alma Redemptoris in Advent |


### Christmas

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3**, **M2/3-N1**, **M3/3-TD** |
|    | Tues, Fri     | **M1/3**, **M2/3-N2**, **M3/3-TD** |
|    | Wed, Sat  | **M1/3**, **M2/3-N3**, **M3/3-TD** |
| Lauds |  | **L-N** |
| Prime |  | **P-N** |
| Terce |  | **T-N** |
| Sext |  | **S-N** |
| None |  | **N-N** |
| Vespers |  | **V-N** |
| Compline |  | **C-N**, Alma Redemptoris in Christmas |

### Christmas in Septuagesima 

for when Septuagesima occurs before 2 February (Purification)


| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3-70**, **M2/3-N1**, **M3/3-TD** |
|    | Tues, Fri     | **M1/3-70**, **M2/3-N2**, **M3/3-TD** |
|    | Wed, Sat  | **M1/3-70**, **M2/3-N3**, **M3/3-TD** |
| Lauds |  | **L-N-70** |
| Prime |  | **P-N-70** |
| Terce |  | **T-N-70** |
| Sext |  | **S-N-70** |
| None |  | **N-N-70** |
| Vespers |  | **V-N-70** |
| Compline |  | **C-N-70**, Alma Redemptoris in Christmas |


### 2 Feb outside of Septuagesima

for when Septuagesima Sunday falls after 2 Feb (Purification)

See Default except with Ave Regina Caelorum instead of Salve Regina

### Septuagesima


| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3-70**, **M2/3-N1**, **M3/3** |
|    | Tues, Fri     | **M1/3-70**, **M2/3-N2**, **M3/3** |
|    | Wed, Sat  | **M1/3-70**, **M2/3-N3**, **M3/3** |
| Lauds |  | **L-70** |
| Prime |  | **P-70** |
| Terce |  | **T-70** |
| Sext |  | **S-70** |
| None |  | **N-70** |
| Vespers |  | **V-70** |
| Compline |  | **C-70**, Ave Regina Caelorum |

### Annunciation in Septuagesima

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3-70**, **M2/3-N1**, **M3/3-A-TD**  |
|    | Tues, Fri     | **M1/3-70**, **M2-3-N2**, **M3/3-A-TD** |
|    | Wed, Sat  | **M1/3-70**, **M2/3-N3-A**, **M3/3-A-TD** |
| Lauds |  | **L-A-70** |
| Prime |  | **P-A-70** |
| Terce |  | **T-A-70** |
| Sext |  | **S-A-70** |
| None |  | **N-A-70** |
| Vespers |  | **V-A-70** |
| Compline |  | **C-A-70**, Alma Redemptoris in Advent |

### Annunciation when tranferred to Easter

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3**, **M2/3-N1**, **M3/3-A-TD**  |
|    | Tues, Fri     | **M1/3**, **M2-3-N2**, **M3/3-A-TD** |
|    | Wed, Sat  | **M1/3**, **M2/3-N3-A**, **M3/3-A-TD** |
| Lauds |  | **L-A** |
| Prime |  | **P-A** |
| Terce |  | **T-A** |
| Sext |  | **S-A** |
| None |  | **N-A** |
| Vespers |  | **V-A** |
| Compline |  | **C-A**, Alma Redemptoris in Advent |



### Triduum

The Little Office is not sung in public during the Sacred Triduum. The Divine Office is pared down at this time and more accessible. Give it a try!

### Easter

Much like Default except with Regina Caeli everywhere.

| Hour      | Variations | Code |
| ----------- | ----------- | ----- |
| Matins   | Sun, Mon, Thurs   | **M1/3**, **M2/3-N1**, **M3/3-TD** |
|    | Tues, Fri     | **M1/3**, **M2/3-N2**, **M3/3-TD** |
|    | Wed, Sat  | **M1/3**, **M2/3-N3**, **M3/3-TD** |
| Lauds |  | **L-P** |
| Prime |  | **P** |
| Terce |  | **T** |
| Sext |  | **S** |
| None |  | **N** |
| Vespers |  | **V-P** |
| Compline |  | **C-P**, Regina Caeli |

### Trinity

Back to the Default settings.



