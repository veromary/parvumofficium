---
title: Officium Parvum Beatae Mariae Virginis
feature_image: "/images/coronationblue.jpg"
feature_text: |
  ## Ave María
---

Collecting information about the Little Office of the Blessed Virgin Mary, particularly how it is sung.

A first step might be to learn [to sing the Little Office of the Immaculate Conception](immac)

You can get my video course on Singing the Little Office in Latin : 

<center>
{% include button.html text="via Gumroad" link="https://gum.co/bvmsing" icon="gumroad" color="#36A9AE" %}
&nbsp; or &nbsp;
{% include button.html text="via Udemy" link="https://www.udemy.com/course/sing-officium-parvum-bmv/?referralCode=7C9068C18692B9588F76" icon="udemy" color="#a435f0" %}
</center>

Gumroad is more for sharing files and gives you access to the videos directly. Udemy is a learning platform which keeps track of which lessons you have watched.

Gumroad also gives you the option to become an affiliate and get a third of the proceeds each time someone signs up for the course using your link. [Sign up here](https://gumroad.com/veromarybrrr/affiliates).

### Latest articles

  <ul class="post-list">
    {% for post in site.posts limit:5 %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>
    <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
      </li>
    {% endfor %}
  </ul>

<p><a href="/blog/">SEE ALL ARTICLES&gt;</a></p>

### Booklets

The latest copies of the booklets ready to print. These are still under development. Please let me know if you find any mistakes or have any suggestions.

**PDFs:** [Matins](/lo/matins.pdf) &bull; [Lauds](/lo/laudes.pdf) &bull; [Prime](/lo/prima.pdf) &bull; [Terce](/lo/terce.pdf) &bull; [Sext](/lo/sext.pdf) &bull; [None](/lo/none.pdf) &bull; [Vespers](/lo/vespers.pdf) &bull; [Compline](/lo/completorium.pdf) or [Old layout](/lo/compline.pdf) &bull; [All the Hours!](/lo/allthehours.pdf)

**ODD PDFs:** [Medieval-ish Lauds](/lo/lauds-medieval.pdf) with extra Psalms and the COmmemoration of Saints as before Pope St Pius X's reform &bull; [Camp Terce](camp-terce.pdf)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Little Office of the Blessed Virgin Mary</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://littleoffice.brandt.id.au" property="cc:attributionName" rel="cc:attributionURL">Veronica Brandt</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://littleoffice.brandt.id.au" rel="dct:source">https://littleoffice.brandt.id.au</a>.<br />Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://littleoffice.brandt.id.au" rel="cc:morePermissions">https://littleoffice.brandt.id.au</a>.

**Recordings (mp3/m4a):** [Lauds](https://anchor.fm/s/6734be28/podcast/play/39925364/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-8-7%2F00f25230-0c8b-c09d-04f6-2bc7d1bf0160.mp3) &bull; [Prime](https://anchor.fm/s/6734be28/podcast/play/38592464/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-11%2F9fe1e2104fd3ba24f9a1482c5277293b.m4a) &bull; [Terce](https://anchor.fm/s/6734be28/podcast/play/40073762/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-7-10%2F61c3cb7b-6725-912b-5972-7eaeb1d17292.mp3) &bull; [Sext](https://anchor.fm/s/6734be28/podcast/play/38593740/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-11%2F9bdd3663fac29d64b10640f7b9995f7f.m4a) &bull; [None](https://anchor.fm/s/6734be28/podcast/play/38596770/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-11%2Fa2df4a9a04e97c82a0e2ca51f0710b91.m4a) &bull; [Vespers](https://anchor.fm/s/6734be28/podcast/play/39007883/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-19%2F82f349c8150772a13877075c10888977.m4a) &bull; [Salve Regina, solemn](https://anchor.fm/s/6734be28/podcast/play/39008025/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-19%2F1ba0d0a14e775c27b9c207308c920579.m4a), [Salve Regina, simple](https://anchor.fm/s/6734be28/podcast/play/38554053/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-10%2Fdc0c75ef10c29f5a385335e573dddd25.m4a), [Regina Caeli, simple](https://anchor.fm/s/6734be28/podcast/play/38553680/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging%2F2021-08-10%2Fac65edf4730e319c0902f1c10d3d4d92.m4a)  &bull;

### Videos:

Although I have kept the complete set of videos for the paid course, I have released quite a few for free on YouTube. [My channel](https://www.youtube.com/channel/UCDkCFFi0iITXWxeF1wbNrEg) also includes videos on hymns, learning Latin and Gregorian chant.

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?list=PLR7cOUX2gl7rc5SoNvS6NrnsDf6ChWuub" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

All my YouTube videos are also backed up at [Odysee](https://odysee.com).
Sometimes I add them to my [Rumble channel](https://rumble.com/c/c-658085) too.

All the Little Office Videos are uploaded to Gumroad and Udemy and you can access those here:

<center>
{% include button.html text="via Gumroad" link="https://gum.co/bvmsing" icon="gumroad" color="#36A9AE" %}
&nbsp; or &nbsp;
{% include button.html text="via Udemy" link="https://www.udemy.com/course/sing-officium-parvum-bmv/?referralCode=7C9068C18692B9588F76" icon="udemy" color="#a435f0" %}
</center>

God bless you!

