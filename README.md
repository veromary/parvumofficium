# Starter kit for [Alembic](https://alembic.darn.es/)

This is a very simple starting point if you wish to use Alembic [as a Jekyll theme gem](https://alembic.darn.es/#as-a-jekyll-theme) or as a [GitHub Pages remote theme](https://github.com/daviddarnes/alembic-kit/tree/remote-theme) (see `remote-theme` branch).

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/daviddarnes/alembic-kit)

or

**[Download the GitHub Pages kit](https://github.com/daviddarnes/alembic-kit/archive/remote-theme.zip)**


### Actual Documentation

240928 - This evening I found lots of failed deploys and learned about updating Ruby. Now the new .ruby-version thing tells Netlify which version to use. On my Debian laptop I followed [these instructions](https://cloudinfrastructureservices.co.uk/how-to-install-ruby-on-rails-on-debian-11-tutorial/)

Sounds like it would be wise to update the theme properly. There seems to be a lot of things that are deprecated.

