\version "2.22.1"

\header {
  title = "Hail Queen of the Angels"
  composer = "from St Basil's Hymnal 1918"
}

global = {
  \key g \major
  \time 3/4
}

melody = \relative c' {
\partial 4 d4 g g b a a c fis, fis a g2
d4 g g b a a c fis, fis a g2
d'4 a a d b b d c c b a2
d,4 g g8( a) b( c) d4 b g c a fis g2 }

alto = \relative c' {
d4 d d g e e e d d fis d2
d4 d d g e e e d d fis d2
g4 a fis a g g b a a g fis2
d4 d g g g g g a fis d d2 }

tenor = \relative c {
d4 b' b d c c a a a c b2
a4 b b d c c a a a c b2
b4 d d d d d d d d d d2
d4 b b8( c) d4 d d b d d c b2 }

bass = \relative c {
d4 g g g c, c c d d d g2 
fis4 g g g c, c c d d d g2 
g4 fis d fis g g g fis fis g d2
d4 g g g8( a) b4 g g d d d g2 }

verseOne = \lyricmode {
Hail, Queen of the Hea -- vens! Hail, Mis -- tress of Earth!
Hail, Vir -- gin most pure, Of im -- ma -- cu -- late birth!
Clear Star of the morn -- ing In beau -- ty en -- shrin'd,
O La -- dy make speed to the help of man -- kind.
}

\score {
  \new ChoirStaff <<
    \new Staff = "women" <<
      \new Voice = "sopranos" {
        \voiceOne
        << \global \melody >>
      }
      \new Voice = "altos" {
        \voiceTwo
        << \global \alto >>
      }
    >>
    \new Lyrics = "verseOne"
   \new Staff = "men" <<
      \clef bass
      \new Voice = "tenors" {
        \voiceOne
        << \global \tenor >>
      }
      \new Voice = "basses" {
        \voiceTwo << \global \bass >>
      }
    >>
    \context Lyrics = "verseOne" \lyricsto "sopranos" \verseOne
  >>
}
